package com.casaroca.weare.server.enumerator;

import java.util.List;
import java.util.function.Function;

import com.casaroca.weare.server.dto.RarityException;

public enum VersionPosition {
	OUTDATED(v -> null), SAME(v -> null), UPGRADABLE(v -> null), HIGHER(v -> {
		final Integer c = v.get(0);
		final Integer h = v.get(1);
		if (c > h) {
			return VersionPosition.OUTDATED;
		}
		return c.equals(h) ? VersionPosition.SAME : VersionPosition.UPGRADABLE;
	}), LESS(v -> {
		final Integer c = v.get(0);
		final Integer l = v.get(2);
		if (c < l) {
			return VersionPosition.OUTDATED;
		}
		return c.equals(l) ? VersionPosition.SAME : VersionPosition.UPGRADABLE;
	}), BETWEEN(v -> {
		final Integer c = v.get(0);
		final Integer h = v.get(1);
		final Integer l = v.get(2);
		if (c > h || c < l) {
			return VersionPosition.OUTDATED;
		} else if (c < h && c > l) {
			return VersionPosition.UPGRADABLE;
		} else if (c.equals(h) && c.equals(l)) {
			return VersionPosition.SAME;
		}
		return c.equals(h) ? VersionPosition.HIGHER : LESS;
	});

	private Function<List<Integer>, VersionPosition> function;

	private VersionPosition(final Function<List<Integer>, VersionPosition> function) {
		this.function = function;
	}

	public VersionPosition compare(final List<Integer> versions) {
		final VersionPosition position = function.apply(versions);
		if (position == VersionPosition.OUTDATED) {
			throw new RarityException(Reply.VERSION_OUTDATED);
		} else if (position == VersionPosition.UPGRADABLE) {
			throw new RarityException(Reply.VERSION_UPGRADABLE);
		}
		return position != VersionPosition.SAME ? position : this;
	}

}
