package com.casaroca.weare.server.enumerator;

public enum Reply {
	OK(0, "OK"), UNKNOWN_EXCEPTION(5, "Tenemos un problema, y no sabemos cuál es."),
	UNFORMATTED_NUMBER(75, "Hay un número que no entendemos."),
	VERSION_OUTDATED(78, "Hay una nueva versión de la app, la que tienes ya está muy desactualizada."),
	VERSION_UPGRADABLE(90, "Hay una nueva versión de la app."),
	OUT_LIMITS(240, "La información necesaria está incompleta."),
	INVALID_ARGUMENT(280, "Recibimos un dato que no es válido."),
	UNREADABLE_MESSAGE(295, "No entendemos lo que se solicitó.");

	private Integer code;
	private String message;

	private Reply(final Integer code, final String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
