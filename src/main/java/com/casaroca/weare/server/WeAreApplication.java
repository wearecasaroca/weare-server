package com.casaroca.weare.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeAreApplication {

	public static void main(final String[] args) {
		SpringApplication.run(WeAreApplication.class, args);
	}

}
