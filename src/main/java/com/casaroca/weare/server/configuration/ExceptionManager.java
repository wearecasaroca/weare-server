package com.casaroca.weare.server.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.casaroca.weare.server.dto.RarityException;
import com.casaroca.weare.server.dto.Response;
import com.casaroca.weare.server.enumerator.Reply;

@ControllerAdvice
public class ExceptionManager {

	private final Logger logger = LoggerFactory.getLogger(ExceptionManager.class);

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response<Void>> exception(final Exception exception) {
		return logResponse(exception, Reply.UNKNOWN_EXCEPTION);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Response<Void>> httpMessageNotReadableException(
			final HttpMessageNotReadableException exception) {
		return logResponse(exception, Reply.UNREADABLE_MESSAGE);
	}

	@ExceptionHandler(IndexOutOfBoundsException.class)
	public ResponseEntity<Response<Void>> indexOutOfBoundsException(final IndexOutOfBoundsException exception) {
		return logResponse(exception, Reply.OUT_LIMITS);
	}

	private ResponseEntity<Response<Void>> logResponse(final Exception exception, final Reply reply) {
		logger.error("Code: {} | Class: {} | Message: {}", reply.getCode(), exception.getClass().getSimpleName(),
				exception.getLocalizedMessage());
		return ResponseEntity.ok(new Response<>(reply));
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Response<Void>> methodArgumentNotValidException(
			final MethodArgumentNotValidException exception) {
		return logResponse(exception, Reply.INVALID_ARGUMENT);
	}

	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<Response<Void>> numberFormatException(final NumberFormatException exception) {
		return logResponse(exception, Reply.UNFORMATTED_NUMBER);
	}

	@ExceptionHandler(RarityException.class)
	public ResponseEntity<Response<Void>> rarity(final RarityException exception) {
		return ResponseEntity.ok(new Response<>(exception.getReply()));
	}

}
