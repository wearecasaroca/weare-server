package com.casaroca.weare.server.dto;

import com.casaroca.weare.server.enumerator.Reply;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
public class Response<T> {

	private Integer code;
	private String message;
	private T body;

	public Response(final Reply reply) {
		this.code = reply.getCode();
		this.message = reply.getMessage();
	}

	public T getBody() {
		return body;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setBody(final T body) {
		this.body = body;
	}

	public void setCode(final Integer code) {
		this.code = code;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

}
