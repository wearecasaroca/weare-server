package com.casaroca.weare.server.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.casaroca.weare.server.dto.Response;
import com.casaroca.weare.server.dto.Version;
import com.casaroca.weare.server.enumerator.Reply;
import com.casaroca.weare.server.enumerator.VersionPosition;

@RestController
@RequestMapping("version")
public class VersionController {

	private static final String VERSION_LATEST = "0.0.1";
	private static final String VERSION_MINIMUM = "0.0.1";

	@PostMapping("validate")
	public Response<Void> validate(@RequestBody @Valid final Version version) {
		final List<Integer> current = splitVersion(version.getTag());
		final List<Integer> higher = splitVersion(VersionController.VERSION_LATEST);
		final List<Integer> less = splitVersion(VersionController.VERSION_MINIMUM);
		VersionPosition position = VersionPosition.BETWEEN;
		for (int i = 0; i <= 2; i++) {
			position = position.compare(Arrays.asList(current.get(i), higher.get(i), less.get(i)));
		}
		return new Response<>(position == VersionPosition.HIGHER ? Reply.OK : Reply.VERSION_UPGRADABLE);
	}

	private List<Integer> splitVersion(final String version) {
		return Arrays.asList(version.split("\\.")).stream().map(Integer::valueOf).collect(Collectors.toList());
	}
}
